import pygame 
import random 
import math 
from pygame import mixer
from colorama import init

pygame.init()

#Medidas e inicialización de la pantalla
screen_width = 700 
screen_height = 500 
screen = pygame.display.set_mode((screen_width, screen_height))


pygame.display.set_caption("Welcome to space\
Invaders Game by: - styles")

#puntaje del juego
score_valor = 0 
scoreX = 5 
scoreY = 5  
font = pygame.font.SysFont('HP Simplified Hans',30)
texto = font.render('Puntaje', True, black) 
screen.blit(texto (10,10)) 

#Sonido 
sonido = pygame.mixer.Sound("Sonidolaser.mp3")

#Gameover 
game_over_font = pygame.font.Font('', 64) 

def mostrar_puntaje(x,y):
    score = font.render("Puntos: " + str(score_valor),True, (255,255,255))
    screen.blit(score, (x,y))
def game_over():
    game_over_text = game_over_font.render("GAME OVER", True, (255,255,255))
    screen.blit(game_over_text, (190, 250))
    
#Jugador 
Imagendeljugador = pygame.image.load()
jugadorX = 370 
JugadorY = 523 
Jugador_Xcambio = 0  

#Invasor 

ImagendelInvasor =[]
Invasor_X = []
Invasor_Y = []
Invasor_Xchange = []
Invasor_Ychange = []

for num in range() :
     ImagendelInvasor.append(pygame.image.load('Alien.pixil'))
     Invasor_X.append(random.randint(64,737))
     Invasor_Y.append(random.randint(30,180))
     Invasor_Xchange.append(1,2)
     Invasor_Ychange.append(50) 
    
bulletImage = pygame.image.load()
bullet_X = 0 
Bullet_Y = 500
bullet_Xchange = 0 
bullet_Ychange = 3 
bullet_state = "rest" 

#Colision de la bala 
def isCollision(x1, x2, y1, y2): 
    distance = math.sqrt((math.pow(x1 - x2,2))) + (math.pow(y1 - y2,2))
    
    if distance <= 50: 
        return True
    else: return False 
    
def player(x, y): 
    screen.blit(Imagendeljugador, (x - 16, y + 10))
def invader(x, y, i): 
    screen.blit(ImagendelInvasor [i], (x,y))

def bullet(x,y): 
    global bullet_state 
    screen.blit(bulletImage, (x,y))
    bullet_state = "fire"
     
                         
 
 
pygame.display.update() 
